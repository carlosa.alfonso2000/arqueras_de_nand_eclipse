package ArcherOfNand;

import java.util.Scanner;

/**
 * Esta clase Arqueras nos dara la opcion de elegir las flechas que tienen, las que pierdan y su estado. 
 * @author Carlos Arredondo Alfonso
 *
 */
public class Arqueras {
	// atributos
	public int flechas;
	public String estado;
	public int perdidas;

	public int getFlechas() {
		return flechas;
	}

	public void setFlechas(int flechas) {
		this.flechas = flechas;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getperdidas() {
		return perdidas;
	}

	public void setperdidas(int perdidas) {
		this.perdidas = perdidas;
	}

	// constructor
	/**
	 * Constructor de Arqueras para las el n�mero de flechas, si son disponibles y
	 * las que se pierden.
	 */
	Arqueras() {
		setFlechas(flechas);
		setEstado("Disponibles");
		setperdidas(perdidas);
	}

//metodos este metodo te lo puedes meter por el culo y vuelas hijo de puta 
	/**
	 * 
	 * 
	 * @param dificultad. Para que al elegir las cartas nos diga que flechas podemos
	 *                    usar y las que perdemos si deben huir las arqueras
	 */
	public void dificultad(int dificultad) {
		switch (dificultad) {
		case 1:
			setFlechas(50);
			setperdidas(-1);
			break;
		case 2:
			setFlechas(48);
			setperdidas(-2);
			break;
		case 3:
			setFlechas(45);
			setperdidas(-3);
			break;
		}
	}

	/**
	 * Metodo para restar flechas despues de que se usen
	 * 
	 */
	public void restarFlechas() {
		setFlechas(flechas - perdidas);
	}

	/**
	 * Metodo con la informaci�n de lo que poseen.
	 */
	public void infoArqueras() {
		System.out.println("n�mero de felchas disponibles: " + flechas);
		System.out.println("n�mero de flechas que se pierden por huida: " + perdidas);
		System.out.println("estado: " + estado);
	}

	public static void main(String[] args) {
		Arqueras arq1 = new Arqueras();

		Scanner sc = new Scanner(System.in);
		int dificultad = 0;
		System.out.println("Introduce dificultad: ");
		System.out.println("1) Facil ");
		System.out.println("2) Media ");
		System.out.println("3) Dificil ");
		dificultad = sc.nextInt();
		arq1.dificultad(dificultad);
		arq1.infoArqueras();

		sc.close();
	}
}
