package ArcherOfNand;

import java.util.*;

/**
 * Clase con la baraja de cartas del juego.
 * @author Carlos Arredondo Alfonso
 *
 */
public class Baraja_Cartas_ArrayList {
	/**
	 * Arraylist que guardara las cartas que tenemos para usar.
	 */
	ArrayList<String> carta = new ArrayList<String>();

	Baraja_Cartas_ArrayList() {

		carta.add("OR");
		carta.add("OR");
		carta.add("OR");
		carta.add("OR");
		carta.add("OR");

		carta.add("OR");
		carta.add("AND");
		carta.add("AND");
		carta.add("AND");
		carta.add("AND");

		carta.add("LIKE");
		carta.add("LIKE");
		carta.add("LIKE");
		carta.add("LIKE");
		carta.add("LIKE");

		carta.add("XOR");
		carta.add("XOR");
		carta.add("XOR");
		carta.add("XOR");
		carta.add("XOR");

		carta.add("COUNT");
		carta.add("COUNT");
		carta.add("COUNT");
		carta.add("COUNT");
		carta.add("COUNT");

		carta.add("NOT");
		carta.add("NOT");
		carta.add("NOT");
		carta.add("NOT");
		carta.add("NOT");

		carta.add("BENGALA");
		carta.add("BENGALA");
		carta.add("BENGALA");

	}

	/**
	 * METODO PARA BARAJAR LAS CARTAS
	 */
	public void barajar() {
		Collections.shuffle(carta);
	}

	/**
	 * METODO PARA MOSTRAR LAS CARTAS
	 */
	public void sacarmano() {
		System.out.println("numero de cartas: " + carta.size());
		System.out.println(carta.get(0));
		System.out.println(carta.get(1));
		System.out.println(carta.get(2));
		System.out.println(carta.get(3));
	}

	// public void removecard() { // metodo para remover las usadas

	public static void main(String[] args) {
		Baraja_Cartas_ArrayList car = new Baraja_Cartas_ArrayList();

		car.barajar();
		car.sacarmano();
	}
}
