package ArcherOfNand;

/**
 * Clase con las ciudades del juego, sus escudos, el estado y el n�mero de orcos y de caballeros. 
 * @author Carlos Arredondo Alfonso
 *
 */
public class Ciudades {

//atributos

	public String nombre;
	public boolean escudoVerde, escudoAzul, escudoRojo;
	public String estado;
	public int num_Orcos;
	public int num_Caballeros;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isescudoVerde() {
		return escudoVerde;
	}

	public void setescudoVerde(boolean escudoVerde) {
		this.escudoVerde = escudoVerde;
	}

	public boolean isescudoAzul() {
		return escudoAzul;
	}

	public void setescudoAzul(boolean escudoAzul) {
		this.escudoAzul = escudoAzul;
	}

	public boolean isescudoRojo() {
		return escudoRojo;
	}

	public void setescudoRojo(boolean escudoRojo) {
		this.escudoRojo = escudoRojo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getNum_Orcos() {
		return num_Orcos;
	}

	public void setNum_Orcos(int num_Orcos) {
		this.num_Orcos = num_Orcos;
	}

	public int getNum_Caballeros() {
		return num_Caballeros;
	}

	public void setNum_Caballeros(int num_Caballeros) {
		this.num_Caballeros = num_Caballeros;
	}

//constructores 
	/**
	 * 
	 * @param nombre
	 * @param escudoRojo
	 * @param escudoVerde
	 * @param escudoAzul
	 */
	Ciudades(String nombre, boolean escudoRojo, boolean escudoVerde, boolean escudoAzul) {
		setNombre(nombre);
		setescudoVerde(escudoVerde);
		setescudoRojo(escudoRojo);
		setescudoAzul(escudoAzul);
		setEstado("Defendida");
		setNum_Orcos(0);
		setNum_Caballeros(0);
	}

	// metodos para sacar informaci�n de cada ciudad y el color que tiene cada una
	// de ellas.
	/**
	 * Es un metodo para que me diga que colores tiene cada ciudad y una linea que
	 * separa una ciudad.
	 */
	public void Escudos() {

		System.out.println("Escudos:");
		if (escudoVerde == true) {
			System.out.println("con escudo verde");

		} else if (escudoVerde == false) {
			System.out.println("sin escudo verde");
		}
		if (escudoAzul == true) {
			System.out.println("con escudo azul");

		} else if (escudoAzul == false) {
			System.out.println("sin escudo azul");
		}
		if (escudoRojo == true) {
			System.out.println("con escudo rojo");

		} else if (escudoRojo == false) {
			System.out.println("sin escudo rojo");
		}
		System.out.println("---------------------------------------");
	}

	// no existe un contador de guerreros porque se hace directamente en el main.
	// *****
	/**
	 * Metodo contador y suma de orcos
	 */
	public void ContOrcos() {
		setNum_Orcos(num_Orcos + 1);
		// System.out.println(num_Orcos);
	}

	/**
	 * Metodo que imprime la informaci�n de la ciudad.
	 */
	public void PrintInfoCiu() { 
		System.out.println(nombre);
		System.out.println("Se encuentra:" + estado);
		System.out.println("N�mero de orcos:" + num_Orcos);
		System.out.println("N�mero de Caballeros:" + num_Caballeros);
	}
	// public void pelea(Ciudades ciudad, int num_Orcos, int num_Caballeros) {
	// if(ciudad == num_Orcos *2 > num_Caballeros) {

	// }
	// }
	/**
	 * Metodo que permite a los orcos y los guerreros pelear. 
	 */
	public void batalla() {
		if (num_Orcos * 2 > num_Caballeros) {
			setNum_Orcos(num_Orcos - (num_Caballeros / 2));
			setNum_Caballeros(0);
		} else {
			setNum_Orcos(0);
			setNum_Caballeros(num_Caballeros - (num_Orcos * 2));
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Ciudades ciu1 = new Ciudades("nombre de la ciudad 1", false, false, false);
		 * Ciudades ciu2 = new Ciudades("nombre de la ciudad 2", true, false, false);
		 * Ciudades ciu3 = new Ciudades("nombre de la ciudad 3", true, true, false);
		 * Ciudades ciu4 = new Ciudades("nombre de la ciudad 4", true, true, true);
		 * Ciudades ciu5 = new Ciudades("nombre de la ciudad 5", false, false, true);
		 * Ciudades ciu6 = new Ciudades("nombre de la ciudad 6", false, true, true);
		 * Ciudades ciu7 = new Ciudades("nombre de la ciudad 7", true, false, true);
		 * 
		 * ciu1.PrintInfoCiu(); ciu1.Escudos(); ciu2.PrintInfoCiu(); ciu2.Escudos();
		 * ciu3.PrintInfoCiu(); ciu3.Escudos(); ciu4.PrintInfoCiu(); ciu4.Escudos();
		 * ciu5.PrintInfoCiu(); ciu5.Escudos(); ciu6.PrintInfoCiu(); ciu6.Escudos();
		 * ciu7.PrintInfoCiu(); ciu7.Escudos();
		 */
	}

}
