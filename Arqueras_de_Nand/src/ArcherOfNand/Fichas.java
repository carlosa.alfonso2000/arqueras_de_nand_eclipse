package ArcherOfNand;

/**
 * Clase que con las fichas que se usaran en el juego.
 * @author Carlos Arredondo Alfonso
 *
 */
public class Fichas {
	// atributos
	public boolean verde;
	public boolean rojo;
	public boolean azul;

	public boolean isVerde() {
		return verde;
	}

	public void setVerde(boolean verde) {
		this.verde = verde;
	}

	public boolean isRojo() {
		return rojo;
	}

	public void setRojo(boolean rojo) {
		this.rojo = rojo;
	}

	public boolean isAzul() {
		return azul;
	}

	public void setAzul(boolean azul) {
		this.azul = azul;
	}

	/**
	 * Metodo para saber el resultado de las monedas para saber que accion se debe
	 * tomar: poner orcos en las ciudades o atacar a las arqueras
	 */
	public void tirarFichas() { // resultado de las monedas para saber que accion se debe tomar si en las
								// ciudades o atacar a las arqueras
		verde = Math.random() < 0.5;
		rojo = Math.random() < 0.5;
		azul = Math.random() < 0.5;

		/*
		 * System.out.println(verde); System.out.println(rojo);
		 * System.out.println(azul);
		 */
	}

	public static void main(String[] args) {
		Fichas ficha1 = new Fichas();

		ficha1.tirarFichas();
	}
}