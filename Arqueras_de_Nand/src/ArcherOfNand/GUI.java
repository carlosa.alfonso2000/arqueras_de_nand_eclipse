package ArcherOfNand;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * Clase para la interfaz que usara el juego.
 * 
 * @author miste
 *
 */
public class GUI extends JFrame implements ActionListener {

	JButton Boton_Facil;
	JButton Boton_Media;
	JButton Boton_Dificil;
	JFrame ventana1;
	JLabel texto1;

	GUI() {

	}

	/**
	 * metodo para generar la ventana con los botones.
	 */
	public void Interfaz() {

		ventana1 = new JFrame();
		Boton_Facil = new JButton();
		Boton_Media = new JButton();
		Boton_Dificil = new JButton();

		Boton_Facil.setText("Facil");
		Boton_Media.setText("Media");
		Boton_Dificil.setText("Dificil");

		ventana1.add(Boton_Facil);
		ventana1.add(Boton_Media);
		ventana1.add(Boton_Dificil);

		Boton_Facil.addActionListener(this);
		Boton_Media.addActionListener(this);
		Boton_Dificil.addActionListener(this);

		ventana1.setTitle("selector de dificultad");
		ventana1.setLayout(new FlowLayout());
		ventana1.setSize(500, 200);
		ventana1.setVisible(true);
		ventana1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == Boton_Facil) {
			// Facil.setText("Modo facil --> 10 Rondas | 50 Flechas | 1 Flechas/Huida");
			System.out.println("Dificultad facil, ser�n 10 Rondas con 50 Flechas y se pierden 1 Flecha por Huida");
			ventana1.dispose();
			nuevaVentanaFacil();

		} else if (e.getSource() == Boton_Media) {
			// Media.setText("Modo media --> 12 Rondas | 48 Flechas | 2 Flechas/Huida");
			System.out.println("Dificultad media, ser�n 12 Rondas con 48 Flechas y se pierden 2 Flecha por Huida");
			ventana1.dispose();
			nuevaVentanaMedia();

		} else if (e.getSource() == Boton_Dificil) {
			// Dificil.setText("Modo dificil --> 15 Rondas | 45 Flechas | 3 Flechas/Huida");
			System.out.println("Dificultad dificil, ser�n 15 Rondas con 45 Flechas y se pierden 3 Flecha por Huida");
			ventana1.dispose();
			nuevaVentanaDificil();
		}

	}

	/**
	 * Metodo que genera una ventana con la informaci�n de la dificultad facil
	 */
	public void nuevaVentanaFacil() {

		/*
		 * ventana = new JFrame(); ventana.setTitle("Modo Facil");;
		 * ventana.setSize(300,120); ventana.setVisible(true); Facil.getText();
		 * System.out.println("Modo facil --> 10 Rondas | 50 Flechas | 1 Flechas/Huida"
		 * ); ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 */
		ventana1 = new JFrame();
		texto1 = new JLabel();
		texto1.setText("Dificultad facil, ser�n 10 Rondas con 50 Flechas y se pierden 1 Flecha por Huida");
		ventana1.add(texto1);
		ventana1.setTitle("Modo Facil");
		ventana1.setSize(500, 200);
		ventana1.setVisible(true);
		ventana1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Metodo que genera una ventana con la informaci�n de la dificultad media
	 */
	public void nuevaVentanaMedia() {

		ventana1 = new JFrame();
		texto1 = new JLabel();
		texto1.setText("Dificultad media, ser�n 12 Rondas con 48 Flechas y se pierden 2 Flecha por Huida");
		ventana1.add(texto1);
		ventana1.setTitle("modo medio");
		ventana1.setSize(500, 200);
		ventana1.setVisible(true);
		ventana1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Metodo que genera una ventana con la informaci�n de la dificultad dificil
	 */
	public void nuevaVentanaDificil() {

		ventana1 = new JFrame();
		texto1 = new JLabel();
		texto1.setText("Dificultad dificil, ser�n 15 Rondas con 45 Flechas y se pierden 3 Flecha por Huida");
		ventana1.add(texto1);
		ventana1.setTitle("Modo dificil");
		ventana1.setSize(500, 200);
		ventana1.setVisible(true);
		ventana1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {

		// SELECTOR DE DIFICULTAD

		GUI vent = new GUI();
		vent.Interfaz();

	}

}