package ArcherOfNand;

import java.util.Scanner;
/**
 * Es una clase que realizara los pasos del juego unicamente. 
 * @author Carlos Arredondo Alfonso
 *
 */
public class Main {

	public static void main(String[] args) {

		Ciudades ciu1 = new Ciudades(" 1) KUF DAL HOLM", false, true, false);
		Ciudades ciu2 = new Ciudades(" 2) KUF STEN VIK", true, false, false);
		Ciudades ciu3 = new Ciudades(" 3) BEK NES VIK", true, true, false);
		Ciudades ciu4 = new Ciudades(" 4) AE NES HOLM", true, true, true);
		Ciudades ciu5 = new Ciudades(" 5) AE STEN SAND", false, false, true);
		Ciudades ciu6 = new Ciudades(" 6) BEK DAL SAND", false, true, true);
		Ciudades ciu7 = new Ciudades(" 7) BEK STEN HOLM", true, false, true);

		Baraja_Cartas_ArrayList car = new Baraja_Cartas_ArrayList();
		Mazo mazo = new Mazo();
		Arqueras arq = new Arqueras();
		Fichas fichas = new Fichas();

		// MENU DIFICULTAD

		int diff = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce dificultad: ");
		System.out.println("	1) Facil ");
		System.out.println("	2) Media ");
		System.out.println("	3) Dificil ");
		diff = sc.nextInt();
		mazo.dificultad(diff);
		arq.dificultad(diff);

		// CARTAS DISPONIBLES
		car.barajar();
		car.sacarmano();
		// COLOCAR GUERRERO

		int posi = 0;
		System.out.println("Donde colocas tu primer guerrero: ");
		System.out.println("	1) KUF DAL HOLM ");
		System.out.println("	2) KUF STEN VIK ");
		System.out.println("	3) BEK NES VIK ");
		System.out.println("	4) AE NES HOLM ");
		System.out.println("	5) AE STEN SAND ");
		System.out.println("	6) BEK DAL SAND ");
		System.out.println("	7) BEK STEN HOLM ");
		posi = sc.nextInt();
		switch (posi) {
		case 1:
			ciu1.setNum_Caballeros(ciu1.num_Caballeros + 1);
			break;
		case 2:
			ciu2.setNum_Caballeros(ciu2.num_Caballeros + 1);
			break;
		case 3:
			ciu3.setNum_Caballeros(ciu3.num_Caballeros + 1);
			break;
		case 4:
			ciu4.setNum_Caballeros(ciu4.num_Caballeros + 1);
			break;
		case 5:
			ciu5.setNum_Caballeros(ciu5.num_Caballeros + 1);
			break;
		case 6:
			ciu6.setNum_Caballeros(ciu6.num_Caballeros + 1);
			break;
		case 7:
			ciu7.setNum_Caballeros(ciu7.num_Caballeros + 1);
			break;
		}

		// TIRAR FICHAS

		for (int tiros = 0; tiros < mazo.NumEnemy[mazo.ronda]; tiros++) {
			fichas.tirarFichas();
			if (fichas.isVerde() == false && fichas.isRojo() == false && fichas.isAzul() == false) {
				arq.setEstado("Huida");
				arq.restarFlechas();
				System.out.println("han huido las arqueras");
			} else if (fichas.isVerde() == false && fichas.isAzul() == true && fichas.isRojo() == false) {
				ciu1.ContOrcos();
			} else if (fichas.isVerde() == true && fichas.isAzul() == false && fichas.isRojo() == false) {
				ciu2.ContOrcos();
			} else if (fichas.isVerde() == true && fichas.isAzul() == true && fichas.isRojo() == false) {
				ciu3.ContOrcos();
			} else if (fichas.isVerde() == true && fichas.isAzul() == true && fichas.isRojo() == true) {
				ciu4.ContOrcos();
			} else if (fichas.isVerde() == false && fichas.isAzul() == false && fichas.isRojo() == true) {
				ciu5.ContOrcos();
			} else if (fichas.isVerde() == false && fichas.isAzul() == true && fichas.isRojo() == true) {
				ciu6.ContOrcos();
			} else if (fichas.isVerde() == true && fichas.isAzul() == false && fichas.isRojo() == true) {
				ciu7.ContOrcos();
			}

			if (ciu1.getNum_Orcos() >= 3) {
				ciu1.setEstado("Invadido");
			} else if (ciu2.getNum_Orcos() >= 3) {
				ciu2.setEstado("invadido");
			} else if (ciu3.getNum_Orcos() >= 3) {
				ciu3.setEstado("invadido");
			} else if (ciu4.getNum_Orcos() >= 3) {
				ciu4.setEstado("invadido");
			} else if (ciu5.getNum_Orcos() >= 3) {
				ciu5.setEstado("invadido");
			} else if (ciu6.getNum_Orcos() >= 3) {
				ciu6.setEstado("invadido");
			} else if (ciu7.getNum_Orcos() >= 3) {
				ciu7.setEstado("invadido");

			}
			arq.infoArqueras();
			mazo.infoRonda();
			ciu1.PrintInfoCiu();
			// ciu1.Escudos();
			ciu2.PrintInfoCiu();
			// ciu2.Escudos();
			ciu3.PrintInfoCiu();
			// ciu3.Escudos();
			ciu4.PrintInfoCiu();
			// ciu4.Escudos();
			ciu5.PrintInfoCiu();
			// ciu5.Escudos();
			ciu6.PrintInfoCiu();
			// ciu6.Escudos();
			ciu7.PrintInfoCiu();
			// ciu7.Escudos();
			mazo.pasarRonda();
			arq.infoArqueras();
			mazo.infoRonda();
			ciu1.PrintInfoCiu();
			// ciu1.Escudos();
			ciu2.PrintInfoCiu();
			// ciu2.Escudos();
			ciu3.PrintInfoCiu();
			// ciu3.Escudos();
			ciu4.PrintInfoCiu();
			// ciu4.Escudos();
			ciu5.PrintInfoCiu();
			// ciu5.Escudos();
			ciu6.PrintInfoCiu();
			// ciu6.Escudos();
			ciu7.PrintInfoCiu();
			// ciu7.Escudos();
			mazo.pasarRonda();

			sc.close();
		}

	}
}
