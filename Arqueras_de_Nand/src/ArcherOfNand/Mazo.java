package ArcherOfNand;

import java.util.Scanner;

/**
 * Con el n�mero de rondas y enemigos. 
 * @author Carlos Arredondo Alfonso
 *
 */
public class Mazo {
	// atributos
	public int NumEnemy[];
	public int ronda;

	public int getRonda() {
		return ronda;
	}

	public void setRonda(int ronda) {
		this.ronda = ronda;
	}

	public int[] getNumEnemy() {
		return NumEnemy;
	}

	public void setNumEnemy(int[] NumEnemy) {
		this.NumEnemy = NumEnemy;
	}

//constructor
	/**
	 * Constructor que recoge el n�mero de enemigos y las rondas
	 */
	Mazo() {
		setNumEnemy(NumEnemy);
		setRonda(0);
	}

//metodos
	/**
	 * 
	 * @param dificultad. El metodo recoge la dificultad para poder saber el n�mero
	 *                    de rondas y de orcos que debe haber en cada ronda
	 */
	public void dificultad(int dificultad) {

		// int CantidadRondas []
		switch (dificultad) {
		case 1:
			int CR1[] = { 3, 3, 3, 3, 3, 4, 4, 4, 4, 4 };
			setNumEnemy(CR1);
			System.out.println("ha elegido: facil");
			break;
		case 2:
			int CR2[] = { 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5 };
			setNumEnemy(CR2);
			System.out.println("ha elegido: medio");
			break;
		case 3:
			int CR3[] = { 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5 };
			setNumEnemy(CR3);
			System.out.println("ha elegido: dificil");
			break;

		}
		// sc.close();
	}

	/**
	 * Es un metodo que nos muestra la informac�on de la ronda.
	 * @return el numero de enemigos en cada ronda
	 */
	public int infoRonda() {
		System.out.println("|--------------------------------------------|");
		System.out.println("** es la ronda: " + ronda++ + " **");
		System.out.println("numero de enemigos por ronda: " + NumEnemy[ronda]);
		return NumEnemy[ronda];
	}
/**
 * un metodo para pasar de ronda.
 */
	public void pasarRonda() {
		// System.out.println(NumEnemy[ronda]);
		ronda = ronda++;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Mazo mazo1 = new Mazo();
		Scanner sc = new Scanner(System.in);
		int dificultad = 0;
		System.out.println("Introduce dificultad: ");
		System.out.println("1) Facil ");
		System.out.println("2) Media ");
		System.out.println("3) Dificil ");
		dificultad = sc.nextInt();
		mazo1.dificultad(dificultad);
		mazo1.infoRonda();
		// System.out.println("xxxxxxxxxxxx");
		mazo1.pasarRonda();
		mazo1.infoRonda();
		sc.close();
	}
}
